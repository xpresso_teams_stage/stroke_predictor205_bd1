#!/usr/bin/env bash
#########################
# This script manages the xpresso environment locally.
########################

USERNAME="admin"
PASSWORD="Abz00ba@123"

DOCKER_HOSTNAME="xpresso"
DOCKER_NAME="xpresso_devenv"
DOCKER_REGISTRY="dockerregistry.xpresso.ai"
DOCKER_IMAGE="dockerregistry.xpresso.ai/library/xpresso_local:2.0.9.rc"
APP_FOLDER="/app"

mounts=""
command=""
project_name="base_xpr"
component_name=""
base_image=${DOCKER_IMAGE}

init(){
    container_name="${DOCKER_NAME}"
    xpresso_host_name="xpresso"
    image_name="${DOCKER_REGISTRY}/local_${HOSTNAME}/xpresso_local_env:1.0"
    if [[ ${project_name} != "" ]];then
      container_name="${container_name}_${project_name}"
      xpresso_host_name="${xpresso_host_name}_${project_name}"
      image_name="${DOCKER_REGISTRY}/${project_name}/xpresso_local_env:1.0"
    fi
    if [[ ${component_name} != "" ]];then
      container_name="${container_name}_${component_name}"
      xpresso_host_name="${xpresso_host_name}_${component_name}"
      image_name="${DOCKER_REGISTRY}/${project_name}/${component_name}/xpresso_local_env:1.0"
    fi

    if [[ ${base_image} == "" ]];then
      base_image=${DOCKER_IMAGE}
    fi

    echo "Container name: ${container_name}"
    echo "Base image: ${base_image}"
    echo "Updated image name: ${image_name}"
    echo "Xpresso host name: ${xpresso_host_name}"

}

push(){

  # Login to docker
  docker login dockerregistry.xpresso.ai -u admin -p Abz00ba@123
  echo "Pushing your image: [${image_name}] to the xpresso registry"
  docker push ${image_name}
}

start(){


    echo "Validating the workspace"
    if [[ "$(docker images -q ${image_name} 2> /dev/null)" == "" ]]; then

      # Login to docker
      docker login dockerregistry.xpresso.ai -u admin -p Abz00ba@123
      docker pull ${base_image} 2> /dev/null
      docker tag ${base_image} ${image_name} 2> /dev/null
    fi

    options="s"
    if [[ "$(docker ps -a | grep ${container_name} 2> /dev/null)" != "" ]]; then
      echo -n "Previous container is still running. Do you want to exec into it/exit existing and start new container/ignore?[e/s/i]: "
      read options
      if [[ ${options} == "s" ]];then
        docker rm -f ${container_name}  2> /dev/null
      elif [[ ${options} == "e" ]];then
        if [[ "$( docker container inspect -f '{{.State.Running}}' ${container_name} )" == "true" ]]; then
          docker exec -it ${container_name} /bin/bash
        else
          echo "Error: Docker container is not running. Please use option 's'. Exiting..."
          exit 1
        fi
      else
        echo "Nothing to do now. Exiting"
        exit 1
       fi
    fi


    if [[ ${options} == "s" ]];then
      echo "Initiating the workspace"
      if [[ ${mounts} == "" ]];then
        docker run -it \
          --hostname=${xpresso_host_name} \
          --name=${container_name} \
          --net=host \
          -v ${PWD}:${APP_FOLDER} \
          ${image_name}
      else
        docker run -it \
            --hostname=${xpresso_host_name} \
            --name=${container_name} \
            --net=host \
            -v ${PWD}:${APP_FOLDER} \
            -v ${mounts} \
            ${image_name}
      fi
    fi

    echo -n "Do you want to persist the changes?[y/n]: "
    read yesorno
    if [[ ${yesorno} == "n" ]];then
      echo "Nothing to do now. Exiting"
      exit 1
    fi

    echo "Persisting your workspace"
    old_image_id=`docker images ${image_name} -q`
    docker commit ${container_name} ${image_name} &> /dev/null

    echo ""
    echo "Workspace has been saved here --> ${image_name}"
    echo ""
    echo "Note: You can now use this as your base image in your Dockerfile"

    docker rmi ${old_image_id} 2> /dev/null
}

while [[ $# -gt 0 ]]; do
   if [[ $1 == *"--"* ]]; then
        param="${1/--/}"
        declare $param="$2"
   fi
  shift
done

init
if [[ ${command} == "start" ]];then
  start
elif [[ ${command} == "push" ]];then
  push
else
  echo "Error: Provide correct parameters: bash start-workspace.sh "
  echo ""
  echo "Supported Command: "
  echo " --command : start/push: Start the workspace for local development"
  echo "                         Push the workspace to the xpresso"
  echo " --base_image: base image to initiate the workspace"
  echo " --project_name: name of the project"
  echo " --component_name: name of the component"
  echo " --mounts: additional mounts"
fi